﻿using CooverBox.Models.Extension;
using CooverBox.Models.Interfaces;
using CooverBox.ViewModels;
using Firebase.Auth;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace CooverBox
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            Routing.RegisterRoute(nameof(Views.MainPage), typeof(Views.MainPage));
            Routing.RegisterRoute(nameof(Views.Price), typeof(Views.Price));
            Routing.RegisterRoute(nameof(Views.Blog), typeof(Views.Blog));
            Routing.RegisterRoute(nameof(Views.AboutUs), typeof(Views.AboutUs));
            Routing.RegisterRoute(nameof(Views.Cabinet.Order), typeof(Views.Cabinet.Order));
            Routing.RegisterRoute(nameof(Views.Cabinet.Design), typeof(Views.Cabinet.Design));
            Routing.RegisterRoute(nameof(Views.Cabinet.Setting), typeof(Views.Cabinet.Setting));
            Routing.RegisterRoute(nameof(Views.AboutProgram), typeof(Views.AboutProgram));

            //AppActions.OnAppAction += AppActions_OnAppAction;
        }

        private async void AppActions_OnAppAction(object sender, AppActionEventArgs e)
        {
            switch (e.AppAction.Id)
            {
                case "Price": await Current.MainPage.Navigation.PushAsync(new Views.Price()); break;
                case "Phone": await Launcher.OpenAsync("tel:+380982692381"); break;
                case "Email": await Launcher.OpenAsync("mailto:stas@cooverbox.in.ua"); break;
                default:
                    break;
            }
        }

        protected async override void OnStart()
        {
            if (Device.RuntimePlatform == Device.Android) DependencyService.Get<ISetStatusBarColor>().SetStatusBarColor();

            //SetAppAction();

            string Email = await SecureStorage.GetAsync("Email");

            if (Email == "" || Email == "null")
            {
                MainPage = new Views.SignIn();
                return;
            }

            SignInViewModel.FirebaseAuthLink = new FirebaseAuthLink(null, new FirebaseAuth
            {
                User = new User()
                {
                    Email = Email,
                }
            });
            MainPage = new Views.SignIn();
        }

        protected async override void OnSleep()
        {
            if (SignInViewModel.FirebaseAuthLink != null)
            {
                if (!SignInViewModel.FirebaseAuthLink.User.CheckForAnonymous())
                {
                    await SecureStorage.SetAsync("Email", SignInViewModel.FirebaseAuthLink.User.Email);
                }
                else
                {
                    FirebaseAuthProvider provider = new FirebaseAuthProvider(SignInViewModel.FirebaseConfig);
                    await provider.DeleteUserAsync(SignInViewModel.FirebaseAuthLink.FirebaseToken);
                    await SecureStorage.SetAsync("Email", "");
                }
            }
        }

        protected override void OnResume()
        {
        }

        private async void SetAppAction()
        {
            try
            {
                await AppActions.SetAsync(
                    new AppAction("Price", "Цены", "Откроет страницу с ценами на товар", "Image1.png"),
                    new AppAction("Phone", "Позвонить нам", "Откроет диалоговое окно с номером для звонка", "Image1.png"),
                    new AppAction("Email", "Написать нам на почту", "Откроет почту с адресом получателя", "Image1.png"));
            }
            catch { }
        }
    }
}
