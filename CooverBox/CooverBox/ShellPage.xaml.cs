﻿
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CooverBox
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ShellPage : Shell
    {
        public ShellPage()
        {
            InitializeComponent();
            if (BindingContext is ViewModels.ShellPageViewModel viewModel) viewModel.Navigation = Navigation;
        }
    }
}