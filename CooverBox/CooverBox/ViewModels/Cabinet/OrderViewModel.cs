﻿using CooverBox.Models;
using CooverBox.Models.Extension;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace CooverBox.ViewModels.Cabinet
{
    internal class OrderViewModel : BaseCabinetViewModel
    {
        public OrderViewModel() : base()
        {
            Timer += OrderViewModel_Timer;
            SetOrders().Await();
        }

        private async void OrderViewModel_Timer()
        {
            if (await Database.GetIsAddInformationAsync())
            {
                await SetOrders();
                Database.SetIsAddInformation();
            }
        }

        public ObservableCollection<Order> Orders { get; set; } = new ObservableCollection<Order>();
        private IEnumerable<Order> SearchOrders { get; set; }
        public bool IsRefreshing { get; set; } = true;
        [System.Obsolete]
        public Command AddOrder => new Command(async () =>
        {
            if (Connectivity.NetworkAccess != NetworkAccess.Internet)
            {
                await Shell.Current.DisplayAlert("Интеренет", "Отсуствует соединение с интеренетом.", "Закрыть");
                return;
            }
            string action = await Shell.Current.DisplayPromptAsync("Добавление заказа", "", "Добавить", "Отмена", "Ведите название заказа", -1, Keyboard.Text);
            RefreshUserDetail.SetDetail("AddOrder", action);
        });

        private async Task SetOrders()
        {
            Client client = await Database.GetClientAsync();
            
            if (client.Orders != null)
            {
                Orders.Clear();
                foreach (Order item in client.Orders)
                {
                    Orders.Add(item);
                }
                OnPropertyChanged(nameof(Orders));
                IsRefreshing = false;
                OnPropertyChanged(nameof(IsRefreshing));
                SearchOrders = new List<Order>(Orders);
            }
        }

        public Command<string> TextChanged => new Command<string>((text) =>
        {
            Orders.Clear();
            if (text == "")
            {
                foreach (Order item in SearchOrders)
                {
                    Orders.Add(item);
                }
                return;
            }

            foreach (Order item in SearchOrders.Where(order => order.Name.ToLower().Contains(text.ToLower())))
            {
                Orders.Add(item);
            }

        });
    }
}