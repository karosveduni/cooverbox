﻿using CooverBox.Models;
using Firebase.Auth;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace CooverBox.ViewModels.Cabinet
{
    internal class SettingViewModel : BaseCabinetViewModel
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string EmailVerified { get; set; }
        public Color SpanColor { get; set; }
        public bool WIFI { get; set; } = true;
        public bool MobileInternet { get; set; } = false;
        public static event Action<string> EventUpdatePhoto;
        public Command Appearing => new Command(async () =>
        {
            Timer += SettingViewModel_Timer;

            WIFI = Preferences.Get(nameof(WIFI), false);
            MobileInternet = Preferences.Get(nameof(MobileInternet), false);
            OnPropertyChanged(nameof(WIFI));
            OnPropertyChanged(nameof(MobileInternet));

            await Initialization();
            await CheckEmailVerified();
        });

        private async void SettingViewModel_Timer()
        {
            if (Preferences.Get("Exit", false) && SignInViewModel.FirebaseAuthLink != null)
            {
                if (Connectivity.NetworkAccess != NetworkAccess.Internet) return;
                FirebaseAuthProvider authProvider = new FirebaseAuthProvider(SignInViewModel.FirebaseConfig);
                SignInViewModel.FirebaseAuthLink.User = await authProvider.GetUserAsync(SignInViewModel.FirebaseAuthLink);

                await Initialization();
                await CheckEmailVerified();
            }
            return;
        }

        private Task Initialization()
        {
            Name = SignInViewModel.FirebaseAuthLink.User.DisplayName;
            Email = SignInViewModel.FirebaseAuthLink.User.Email;
            OnPropertyChanged(nameof(Name));
            OnPropertyChanged(nameof(Email));
            return Task.CompletedTask;
        }

        private Task CheckEmailVerified()
        {
            EmailVerified = !SignInViewModel.FirebaseAuthLink.User.IsEmailVerified ? "Вы не подтвердили свою почту." : "Ваша почта подтверждена";
            SpanColor = SignInViewModel.FirebaseAuthLink.User.IsEmailVerified ? Color.LightGreen : Color.Red;
            OnPropertyChanged(nameof(EmailVerified));
            OnPropertyChanged(nameof(SpanColor));
            return Task.CompletedTask;
        }

        public Command ResetPassword => new Command(async () =>
        {
            if (Connectivity.ConnectionProfiles.Any(cp => cp == ConnectionProfile.Unknown))
            {
                await Shell.Current.DisplayAlert("Интеренет", "Отсуствует соединение с интеренетом.", "Закрыть");
                return;
            }
            FirebaseAuthProvider provider = new FirebaseAuthProvider(SignInViewModel.FirebaseConfig);
            await provider.SendPasswordResetEmailAsync(SignInViewModel.FirebaseAuthLink.User.Email);
            await Shell.Current.DisplayAlert("Сброс пароля", "На вашу почту прийдет письмо для сброса пароля.", "Закрыть");
        });

        [Obsolete]
        public Command RefreshDetails => new Command<string>(async (s) =>
        {
            if (Connectivity.ConnectionProfiles.Any(cp => cp == ConnectionProfile.Unknown))
            {
                await Shell.Current.DisplayAlert("Интеренет", "Отсуствует соединение с интеренетом.", "Закрыть");
                return;
            }
            string action = null;
            switch (s)
            {
                case "Email":
                    action = await Shell.Current.DisplayPromptAsync("Обновление данных пользователей", "", "Изменить", "Отмена", "Ведите новую почту", -1, Keyboard.Email);
                    break;
                case "Name":
                    action = await Shell.Current.DisplayPromptAsync("Обновление данных пользователей", "", "Изменить", "Отмена", "Ведите новое имя", -1, Keyboard.Text);
                    break;
                default:
                    break;
            }

            RefreshUserDetail.SetDetail(s, action);
        });
        public Command<string> Toggled => new Command<string>((s) =>
        {
            if (s == "WIFI") Preferences.Set(nameof(WIFI), WIFI);
            if (s == "MobileInternet") Preferences.Set(nameof(MobileInternet), MobileInternet);
        });

        [Obsolete]
        public Command UpdatePhoto => new Command(async () =>
        {
            if (Connectivity.NetworkAccess != NetworkAccess.Internet)
            {
                await Shell.Current.DisplayAlert("Интернет", "Отсуствует интернет.", "Закрыть");
                return;
            }
            string url = "";
            string result = await Application.Current.MainPage.DisplayActionSheet("Загрузка аватара", "Отмена", null, "Выбрать аватарку на устройстве", "Загрузить по ссылке(URL)");
            switch (result)
            {
                case "Выбрать аватарку на устройстве":
                    FileResult file = await MediaPicker.PickPhotoAsync();
                    if (file == null) return;

                    url = await Database.UpdatePhotoAsync(await file.OpenReadAsync());

                    break;
                case "Загрузить по ссылке(URL)":
                    url = await Application.Current.MainPage.DisplayPromptAsync("Загрузка аватара", "Ведите ссылку", "Загрузить аватарку", "Отмена", "URL", -1, Keyboard.Url);

                    if (url == null || url == "") return;

                    Stream stream = GetStreamFromUrl(url);
                    if (stream != null)
                    {
                        url = await Database.UpdatePhotoAsync(stream);
                    }
                    else
                    {
                        await Application.Current.MainPage.DisplayAlert("Ошибка", "Ведённая вами ссылка имеет не правильный формат", "Закрыть");
                        return;
                    }

                    break;
                default:
                    return;
            }

            FirebaseAuthProvider provider = new FirebaseAuthProvider(SignInViewModel.FirebaseConfig);
            _ = await provider.UpdateProfileAsync(SignInViewModel.FirebaseAuthLink.FirebaseToken, SignInViewModel.FirebaseAuthLink.User.DisplayName, url);
            EventUpdatePhoto.Invoke(url);

            await Shell.Current.DisplayAlert("Фотография", "Фотография успешна обновлена", "Закрыть");
        });

        public static Stream GetStreamFromUrl(string url)
        {
            byte[] imageData = null;
            Stream stream;

            try
            {
                using (System.Net.WebClient wc = new System.Net.WebClient())
                {
                    imageData = wc.DownloadData(url);
                }
                stream = new MemoryStream(imageData);
                return stream;
            }
            catch { return null; }

        }

    }
}