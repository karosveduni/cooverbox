﻿using CooverBox.Models;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace CooverBox.ViewModels.Cabinet
{
    internal class DesignViewModel : BaseCabinetViewModel
    {
        public DesignViewModel() : base()
        {
            Timer += DesignViewModel_Timer;
        }

        private async void DesignViewModel_Timer()
        {
            if (await Database.GetIsAddInformationAsync())
            {
                await SetDesign();
                Database.SetIsAddInformation();
            }
        }

        public ObservableCollection<Design> Designs { get; set; } = new ObservableCollection<Design>();
        public IEnumerable<Design> SearchDesign { get; set; }
        public bool IsRefreshing { get; set; } = true;
        private async Task SetDesign()
        {
            bool result = Device.RuntimePlatform == Device.Android ? Connectivity.ConnectionProfiles.Contains(ConnectionProfile.WiFi) : Connectivity.ConnectionProfiles.Contains(ConnectionProfile.Ethernet);
            if ((result && Preferences.Get("WIFI", false)) || (Connectivity.ConnectionProfiles.Contains(ConnectionProfile.Cellular) && Preferences.Get("MobileInternet", false)))
            {
                try
                {
                    IEnumerable<Design> designs = await Database.GetDesignsAsync();
                    foreach (Design item in designs)
                    {
                        System.Diagnostics.Debug.WriteLine(item.URL);
                        Designs.Add(item);
                    }
                }
                catch (System.Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(e.Message);
                }
            }
            
            IsRefreshing = false;
            OnPropertyChanged(nameof(IsRefreshing));
        }
        public Command Appearing => new Command(async () =>
        {
            await SetDesign();
        });
        public Command Refreshing => new Command(() =>
        {
            IsRefreshing = false;
            OnPropertyChanged(nameof(IsRefreshing));
        });

        [System.Obsolete]
        public Command AddDesign => new Command<string>(async (s) =>
        {
            string url = null;
            switch (s)
            {
                case "Create":
                    break;
                case "URL":
                    url = await Application.Current.MainPage.DisplayPromptAsync("URL", "Ведите ссылку на фото", "Загрузить", "Отмена", "Ведите ссылку", -1, Keyboard.Url);
                    if (url != "" && url != null)
                    {
                        url = await Database.AddDesigenAsync(url);
                        RefreshUserDetail.SetDetail("AddDesign", url);
                    }
                    break;
                case "Gallery":
                    FileResult file = await MediaPicker.PickPhotoAsync();
                    if (file != null)
                    {
                        url = await Database.AddDesigenAsync(await file.OpenReadAsync());
                        RefreshUserDetail.SetDetail("AddDesign", url);
                    }
                    break;
                default:
                    break;
            }
        });
    }
}