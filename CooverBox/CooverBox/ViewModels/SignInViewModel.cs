﻿using CooverBox.Models;
using Firebase.Auth;
using System;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace CooverBox.ViewModels
{
    internal class SignInViewModel : BaseViewModel
    {
        public SignInViewModel()
        {
            Email = FirebaseAuthLink?.User?.Email;
        }
        public const string WebAPIkey = "AIzaSyDe9yJQG7bRvthFOq4RJNb8LUgANIo7JH8";
        public static readonly FirebaseConfig FirebaseConfig = new FirebaseConfig(WebAPIkey);
        public static FirebaseAuthLink FirebaseAuthLink;
        public string Email { get; set; }
        public string EmailRegistration { get; set; }
        public string Password { get; set; }
        public bool IsPasswordEnter { get; set; } = true;
        public bool IsPasswordRegistration { get; set; } = true;
        public string Nickname { get; set; }
        public string SignInTitle { get; set; } = null;
        public bool ButtonIsEnabled { get; set; } = true;
        public bool IsRunning { get; set; } = false;
        public Command OpenPage => new Command<string>(async (s) =>
        {
            if (Connectivity.NetworkAccess != NetworkAccess.Internet)
            {
                await Application.Current.MainPage.DisplayAlert("Интеренет", "Отсуствует соединение с интеренетом.", "Закрыть");
                return;
            }

            if (string.IsNullOrEmpty(Password))
            {
                await Application.Current.MainPage.DisplayAlert("Ввод данных", "Вы не вели пароль", "Закрыть");
                return;
            }

            _ = bool.TryParse(s, out bool Enter);
            if (Enter)
            {
                if (string.IsNullOrEmpty(Email))
                {
                    await Application.Current.MainPage.DisplayAlert("Ввод данных", "Вы не вели Email", "Закрыть");
                    return;
                }
                try
                {
                    ButtonIsEnabled = false;
                    OnPropertyChanged(nameof(ButtonIsEnabled));
                    IsRunning = true;
                    OnPropertyChanged(nameof(IsRunning));
                    FirebaseAuthProvider provider = new FirebaseAuthProvider(FirebaseConfig);
                    FirebaseAuthLink authLink = await provider.SignInWithEmailAndPasswordAsync(Email, Password);
                    FirebaseAuthLink = await authLink.GetFreshAuthAsync();
                    ButtonIsEnabled = true;
                    OnPropertyChanged(nameof(ButtonIsEnabled));
                    IsRunning = false;
                    OnPropertyChanged(nameof(IsRunning));
                }
                catch (Exception ex)
                {
                    if (Device.RuntimePlatform == Device.Android) Vibration.Vibrate();

                    if (ex.Message.Contains("INVALID_EMAIL") || ex.Message.Contains("EMAIL_NOT_FOUND") || ex.Message.Contains("INVALID_PASSWORD"))
                    {
                        await Application.Current.MainPage.DisplayAlert("Ошибка", "Проверьте почту и пароль. Пользователь не найден.", "Закрыть окно");
                    }
                    ButtonIsEnabled = true;
                    OnPropertyChanged(nameof(ButtonIsEnabled));
                    IsRunning = false;
                    OnPropertyChanged(nameof(IsRunning));
                    return;
                }
            }
            else
            {
                if (string.IsNullOrEmpty(Nickname))
                {
                    await Application.Current.MainPage.DisplayAlert("Ввод данных", "Ведите свое имя", "Закрыть");
                    return;
                }
                if (string.IsNullOrEmpty(EmailRegistration))
                {
                    await Application.Current.MainPage.DisplayAlert("Ввод данных", "Вы не вели Email", "Закрыть");
                    return;
                }
                try
                {
                    ButtonIsEnabled = false;
                    OnPropertyChanged(nameof(ButtonIsEnabled));
                    IsRunning = true;
                    OnPropertyChanged(nameof(IsRunning));
                    FirebaseAuthProvider authProvider = new FirebaseAuthProvider(new FirebaseConfig(WebAPIkey));
                    FirebaseAuthLink auth = await authProvider.CreateUserWithEmailAndPasswordAsync(EmailRegistration, Password, Nickname, true);
                    FirebaseAuthLink = await auth.GetFreshAuthAsync();

                    Database.SetClient(new Client
                    {
                        Designs = Array.Empty<Design>(),
                        IsAddInformation = false,
                        Orders = Array.Empty<Order>(),
                    });
                    ButtonIsEnabled = true;
                    OnPropertyChanged(nameof(ButtonIsEnabled));
                    IsRunning = false;
                    OnPropertyChanged(nameof(IsRunning));
                }
                catch (Exception ex)
                {
                    if(Device.RuntimePlatform == Device.Android) Vibration.Vibrate();

                    if (ex.Message.Contains("INVALID_EMAIL") || ex.Message.Contains("WEAK_PASSWORD") || ex.Message.Contains("EMAIL_EXIST") || ex.Message.Contains("MISSING_EMAIL"))
                    {
                        await Application.Current.MainPage.DisplayAlert("Ошибка", "Проверьте ввод данных.\nПароль не меньше 6 символов.\nФормат почты.\nИли пользователь с такой почтой уже существует.", "Закрыть окно");
                    }
                    ButtonIsEnabled = true;
                    OnPropertyChanged(nameof(ButtonIsEnabled));
                    IsRunning = false;
                    OnPropertyChanged(nameof(IsRunning));
                    return;
                }
            }

            Application.Current.MainPage = new ShellPage();

        });
        public Command ShowPassword => new Command<string>((s) =>
        {
            if (s == "Enter")
            {
                IsPasswordEnter = !IsPasswordEnter;
                OnPropertyChanged(nameof(IsPasswordEnter));
            }
            if (s == "Registration")
            {
                IsPasswordRegistration = !IsPasswordRegistration;
                OnPropertyChanged(nameof(IsPasswordRegistration));
            }
        });
        public Command CurrentPageChanged => new Command(() =>
        {
            if (SignInTitle == null || SignInTitle == "Регистрация")
                SignInTitle = "Вход";
            else SignInTitle = "Регистрация";
            OnPropertyChanged(nameof(SignInTitle));
        });

        public Command SignInAnonymous => new Command(async () =>
        {
            FirebaseAuthProvider provider = new FirebaseAuthProvider(FirebaseConfig);
            FirebaseAuthLink = await provider.SignInAnonymouslyAsync();
            FirebaseAuthLink = await FirebaseAuthLink.GetFreshAuthAsync();

            Application.Current.MainPage = new ShellPage();
        });

        [Obsolete]
        public Command ResetPassword => new Command(async () =>
        {
            string Email = await Application.Current.MainPage.DisplayPromptAsync("Сброс пароля", "Ведите свой email", "Сброс пароля", "Отмена", "Ведите email", -1, Keyboard.Email);
            FirebaseAuthProvider provider = new FirebaseAuthProvider(FirebaseConfig);
            if (Email != null && Email != "")
            {
                try
                {
                    await provider.SendPasswordResetEmailAsync(Email);
                }
                catch { }
            }
        });
    }
}
