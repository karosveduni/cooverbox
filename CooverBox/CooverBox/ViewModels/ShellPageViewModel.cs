﻿using CooverBox.Models;
using Xamarin.Essentials;
using Xamarin.Forms;
using CooverBox.Models.Extension;

namespace CooverBox.ViewModels
{
    internal class ShellPageViewModel : BaseViewModel
    {
        public ShellPageViewModel()
        {
            IsVisible = !SignInViewModel.FirebaseAuthLink.User.CheckForAnonymous();
            Cabinet.SettingViewModel.EventUpdatePhoto += SettingViewModel_EventUpdatePhoto;
            Name = SignInViewModel.FirebaseAuthLink.User.CheckForAnonymous() ? SignInViewModel.FirebaseAuthLink.User.LocalId : SignInViewModel.FirebaseAuthLink.User.DisplayName;
            ImageSource =  SignInViewModel.FirebaseAuthLink.User.PhotoUrl;
        }

        private void SettingViewModel_EventUpdatePhoto(string obj)
        {
            ImageSource = ImageSource.FromUri(new System.Uri(obj));
            OnPropertyChanged(nameof(ImageSource));
        }
        public bool IsVisible { get; set; }
        public string Name { get; set; } = "";
        public string TextButton { get; set; } = "Вход в аккаунт";
        public ImageSource ImageSource { get; set; } = null;
        public Command OpenPopup => new Command(async () =>
        {
            string action = await Application.Current.MainPage.DisplayActionSheet("Контактные данные", "Закрыть", null, "Telegram", "+380982692381", "+380938345901", "stas@cooverbox.in.ua");
            switch (action)
            {
                case "Telegram": await Launcher.OpenAsync("https://t.me/irindi"); break;
                case "+380982692381":
                case "+380938345901":
                    await Launcher.OpenAsync($"tel:{action}"); break;
                case "stas@cooverbox.in.ua": await Launcher.OpenAsync($"mailto:{action}"); break;
                default:
                    break;
            }
        });
        public Command OpenSite => new Command<string>(async (s) => await Launcher.OpenAsync(s));
        public Command SignIn => new Command(async () => await Shell.Current.Navigation.PushAsync(new Views.SignIn(), true));
        public Command SignOut => new Command(async () =>
        {
            if (SignInViewModel.FirebaseAuthLink.User.CheckForAnonymous())
            {
                Firebase.Auth.FirebaseAuthProvider provider = new Firebase.Auth.FirebaseAuthProvider(SignInViewModel.FirebaseConfig);
                await provider.DeleteUserAsync(SignInViewModel.FirebaseAuthLink.FirebaseToken);
            }
            Application.Current.MainPage = new Views.SignIn();
        });

        public Command OpenSetting => new Command(() =>
        {
            if (IsVisible)
            {
                _ = Shell.Current.GoToAsync(nameof(Views.Cabinet.Setting));
            }
        });

        public Command OpenOrder => new Command(() => Shell.Current.GoToAsync(nameof(Views.Cabinet.Order), true));

        public Command OpenDesignt => new Command(() => Shell.Current.GoToAsync(nameof(Views.Cabinet.Design), true));
    }
}
