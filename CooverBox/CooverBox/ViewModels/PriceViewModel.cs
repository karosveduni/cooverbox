﻿using CooverBox.Models;
using System;
using System.Collections.ObjectModel;
using Xamarin.Forms;

namespace CooverBox.ViewModels
{
    internal class PriceViewModel : BaseViewModel
    {
        public double Value { get; set; } = 50;
        public int Min { get; } = 50;

        private string selectedItem = "Книга";
        public string SelectedItem
        {
            get => selectedItem;
            set
            {
                selectedItem = value;
                InitializationArraySize();
            }
        }
        public int SelectedIndexPrintType { get; set; } = 2;
        public int SelectedIndexExcipients { get; set; } = 3;
        public int SelectedIndexExpirationDate { get; set; } = 0;
        public int SelectedIndexPayment { get; set; } = 0;

        public string SelectItemArraySize { get; set; }
        public ObservableCollection<string> ArraySize { get; set; } = new ObservableCollection<string>();
        private void InitializationArraySize()
        {

            switch (selectedItem)
            {
                case "Книга":
                    ArraySize = new ObservableCollection<string>(new string[] {
                        "70x70x40", "60x60x20", "90x90x40", "90x90x25", "96x96x20", "80x80x80", "115x90x20", "121x96x12", "103x103x32", "108x108x19", "103x103x60", "150x110x25",
                        "120x120x40", "192x55x55", "140x90x60", "180x70x25", "167x115x37", "168x168x32", "150x150x50", "165x150x35", "171x156x25", "180x180x50", "190x190x20",
                        "220x95x20", "170x180x60", "220x50x30", "175x195x50", "215x130x20", "205x175x50", "197x140x85", "180x120x110", "210x145x40", "200x140x60", "220x162x50",
                        "230x150x90", "184x140x97", "258x182x55", "230x215x85", "290x190x95", "257x196x85", "260x225x30", "255x205x40", "255x210x25", "230x230x55", "280x95x105",
                        "250x200x90", "275x210x90", "250x250x50", "281x216x40", "285x225x40", "260x260x68", "280x230x60", "280x250x50", "280x280x18", "284x284x38", "320x120x120",
                        "230x300x85", "345x115x48", "300x150x50", "350x110x40", "302x214x45", "300x250x65", "330x210x85", "364x230x90", "370x237x40", "356x256x40", "350x250x110",
                        "300x300x50", "350x350x70", "400x300x90", "400x300x60", "415x220x50", "500x150x80", "460x310x105", "500x205x35", "500x205x50", "550x350x100", "600x250x100"});

                    break;
                case "Кубик":
                    ArraySize = new ObservableCollection<string>(new string[] { "80x80x80", "110x110x110", "140x140x140", "240x240x240" });
                    break;
                case "Шкатулка":
                    ArraySize = new ObservableCollection<string>(new string[] {
                        "90x90x45","50x135x50","100x100x100","80x160x50","125x145x55","128x178x54","110x240x120","178x258x74","150x280x120","160x220x54","230x230x90","90x310x90",
                        "180x180x260","200x320x100","230x320x105","238x338x94","280x360x90","300x300x180" });
                    break;
                case "Крафт":
                    ArraySize = new ObservableCollection<string>(new string[] {
                        "40x40x40","70x70x30","80x80x50","80x80x80","50x110x55","80x160x60","75x150x25","60x140x30","120x120x50","105x150x25","120x120x80","185x32x28","55x190x55","220x45x30",
                        "125x210x45","170x110x35","170x190x35","180x180x60","130x255x80","170x170x120","200x200x60","220x220x50","240x200x84","230x230x90","195x262x100","250x250x140","150x280x40",
                        "152x272x75","195x275x60","180x300x70","190x300x100","350x110x40", "325x220x105","300x230x80","340x260x96","300x300x90","390x300x90","400x170x120" });
                    break;
                case "Слайдер":
                    ArraySize = new ObservableCollection<string>(new string[] {
                        "70x70x40","60x60x20","90x90x40","90x90x25","96x96x20","80x80x80","115x90x20","121x96x12","103x103x32","108x108x19","103x103x60","150x110x25","120x120x40","192x55x55",
                        "140x90x60","115x167x37","168x168x32","150x150x50","165x150x35","171x156x25","180x180x50","220x95x20","170x180x60","220x50x30","175x195x50","215x130x20","205x175x50","197x140x85",
                        "180x120x110","210x145x40","220x162x50","258x182x55","230x215x85","290x190x95","257x196x85","260x225x30","255x205x40","230x230x55","280x95x105","250x200x90","275x210x90","250x250x50",
                        "281x216x40","285x225x40","260x260x68","280x230x60","280x250x50","280x280x18","284x284x38","320x120x120","230x300x85","300x250x65","330x210x85","364x230x90","370x237x40","356x256x40",
                        "350x250x110" });
                    break;
                case "Сумка":
                    ArraySize = new ObservableCollection<string>(new string[] { "90x380x380", "70x250x170" });
                    break;
                default:
                    break;
            }

            OnPropertyChanged(nameof(ArraySize));

            SelectItemArraySize = ArraySize[0];
            OnPropertyChanged(nameof(SelectItemArraySize));

        }

        public Command Focused => new Command<Entry>((Entry) =>
        {
            Entry.Text = Entry.Text.Split(' ')[0];
        });

        public Command Unfocused => new Command<object[]>(async (values) =>
        {
            if (values[0] is Entry Entry && values[1] is Slider Slider)
            {
                if (Entry.Text == "")
                {
                    await Application.Current.MainPage.DisplayAlert("Пустая строка", "Ведите количество коробок для заказа.", "ОК");
                    _ = Entry.Focus();
                    return;
                }

                Slider.Value = Convert.ToDouble(Entry.Text);
            }
        });

        public double Price { get; set; } = 4812.5;
        public int PrintType { get; set; }
        public int Excipients { get; set; }
        public int ExpirationDate { get; set; }
        public int Payment { get; set; }
        private readonly Models.Convert.PriceForOne PFO = new Models.Convert.PriceForOne();
        public Command ValueChanged => new Command(() => CalculatingPrice(Value, Math.Round(Convert.ToDouble(PFO.Convert(Value, null, "грв/шт", null).ToString().Split(' ')[0]) / 100, 1)));
        public Command CheckedChanged => new Command<string>((text) => CalculatingPrice(Value, Math.Round(Convert.ToDouble(text.Split(' ')[0]) / 100, 1)));

        private void CalculatingPrice(double value, double PriceForOne)
        {
            Price = (value * PriceForOne) + PrintType + (value * Excipients);

            if (ExpirationDate != 0)
            {
                Price += Price * ExpirationDate / 100;
            }

            if (Payment != 0)
            {
                Price += Price * Payment / 100;
            }

            OnPropertyChanged(nameof(Price));
        }

        public Command<int> SelectedIndexChanged => new Command<int>((SelectedIndex) =>
        {
            switch (SelectedIndex)
            {
                case 0: PrintType = 0; break;
                case 1: PrintType = 500; break;
                case 2: PrintType = 0; break;
                default:
                    break;
            }
            SetCalculating();
        });

        private void SetCalculating()
        {
            string str = PFO.Convert(Value, null, "грв/шт", null).ToString();
            double PriceForOne = Convert.ToDouble(str.Split(' ')[0]) / 100;
            CalculatingPrice(Value, PriceForOne);
        }

        public Command<int> SelectedIndexChangedExcipients => new Command<int>((SelectedIndex) =>
        {
            switch (SelectedIndex)
            {
                case 0: Excipients = 4; break;
                case 1: Excipients = 7; break;
                case 2: Excipients = 10; break;
                case 3: Excipients = 0; break;
                default:
                    break;
            }
            SetCalculating();
        });
        public Command<int> SelectedIndexChangedExpirationDate => new Command<int>((SelectedIndex) =>
        {
            switch (SelectedIndex)
            {
                case 0: ExpirationDate = 0; break;
                case 1: ExpirationDate = 20; break;
                default:
                    break;
            }
            SetCalculating();
        });
        public Command<int> SelectedIndexChangedPayment => new Command<int>((SelectedIndex) =>
        {
            switch (SelectedIndex)
            {
                case 0: Payment = 0; break;
                case 1: Payment = 20; break;
                default:
                    break;
            }
            SetCalculating();
        });

        
    }
}