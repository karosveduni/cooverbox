﻿using CooverBox.Models;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace CooverBox.ViewModels
{
    class AbouteUsViewModel : BaseViewModel
    {
        public ICommand OpenLinkTelephone => new Command<string>(async (s) =>
        {
            await Launcher.OpenAsync(s);
        });

        public ICommand OpenEmail => new Command<string>(async (s) =>
        {
            await Launcher.OpenAsync("mailto:stas@cooverbox.in.ua?subject=У меня есть вопрос&body=Помогите мне со следующим.");
        });

        public ICommand OpenMaps => new Command(async () =>
        {
            await Map.OpenAsync(new Placemark()
            {
                Locality = "CooverBox",
            });
        });

        public ICommand OpenSocialNetworks => new Command<string>(async (link) =>
        {
            await Browser.OpenAsync(link);
        });

    }
}
