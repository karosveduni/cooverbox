﻿using CooverBox.Models;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace CooverBox.ViewModels
{
    class BlogViewModel : BaseViewModel
    {
        public ICommand OpenHyperLink => new Command<string>(async (s) =>
        {
            await Launcher.OpenAsync(s);
        });
    }
}
