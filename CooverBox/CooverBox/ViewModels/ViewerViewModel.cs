﻿using CooverBox.Models;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace CooverBox.ViewModels
{
    internal class ViewerViewModel : BaseViewModel
    {
        public ViewerViewModel() { }
        public ViewerViewModel(IEnumerable<Design> Designs)
        {
            this.Designs = new ObservableCollection<Design>(Designs);
        }

        public ObservableCollection<Design> Designs { get; set; }
    }
}
