﻿using CooverBox.Models;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace CooverBox.ViewModels
{
    internal class AbourProgramViewModel : BaseViewModel
    {
        public Command<string> HyperLink => new Command<string>(async (Hyperlink) => await Launcher.OpenAsync(Hyperlink));
    }
}
