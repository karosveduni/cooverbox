﻿
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CooverBox.Views.Cabinet
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Design : ContentPage
    {
        public Design()
        {
            InitializeComponent();
        }
    }
}