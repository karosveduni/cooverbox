﻿
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CooverBox.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SignIn : TabbedPage
    {
        public SignIn()
        {
            InitializeComponent();
            if (BindingContext is ViewModels.SignInViewModel viewModel) viewModel.Navigation = Navigation;
        }
    }
}