﻿
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CooverBox.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AboutProgram : ContentPage
    {
        public AboutProgram()
        {
            InitializeComponent();
        }
    }
}