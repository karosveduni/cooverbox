﻿
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CooverBox.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AboutUs : ContentPage
    {
        public AboutUs()
        {
            InitializeComponent();
        }
    }
}