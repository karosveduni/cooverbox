﻿using CooverBox.Models;
using System.Collections.Generic;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CooverBox.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Viewer : ContentPage
    {
        public Viewer()
        {
            InitializeComponent();
        }
        public Viewer(IEnumerable<Design> Designs)
        {
            InitializeComponent();

            BindingContext = new ViewModels.ViewerViewModel(Designs);
        }
    }
}