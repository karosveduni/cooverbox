﻿using System.Threading.Tasks;

namespace CooverBox.Models.Extension
{
    public static class TaskExtension
    {
        public static async void Await<T>(this Task<T> task)
        {
            _ = await task;
        }
        public static async void Await(this Task task)
        {
            await task;
        }
    }
}
