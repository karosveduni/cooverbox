﻿namespace CooverBox.Models.Extension
{
    internal static class ClientExtension
    {
        public static bool CheckForAnonymous(this Firebase.Auth.User user)
        {
            return user.Email == "";
        }
    }
}
