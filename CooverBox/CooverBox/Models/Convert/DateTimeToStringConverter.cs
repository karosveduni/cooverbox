﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace CooverBox.Models.Convert
{
    internal class DateTimeToStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            _ = DateTime.TryParse(value.ToString(), out DateTime datetime);
            DateTime datetime1 = new DateTime(1, 1, 1, 0, 0, 0, 0);
            return datetime1.ToString() == datetime.ToString() ? "(Заказ еще выполняется)" : value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
