﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace CooverBox.Models.Convert
{
    class PriceForOne : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            double quantity = (double)value;
            if (quantity < 100) return $"96,25 {parameter}";
            if (quantity < 150) return $"85,25 {parameter}";
            if (quantity < 200) return $"82,5 {parameter}";
            if (quantity < 300) return $"79,75 {parameter}";
            if (quantity < 400) return $"74,25 {parameter}";
            if (quantity < 500) return $"71,5 {parameter}";
            if (quantity < 1000) return $"68,75 {parameter}";

            return $"63,25 {parameter}";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
