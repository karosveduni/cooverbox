﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace CooverBox.Models.Convert
{
    class AddTextForNumber : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return $"{Math.Round((double)value)} {parameter}";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
