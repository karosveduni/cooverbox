﻿using System.Collections.Generic;

namespace CooverBox.Models
{
    internal class Client
    {
        public bool IsAddInformation { get; set; }
        public IEnumerable<Order> Orders { get; set; }
        public IEnumerable<Design> Designs { get; set; }
    }
}
