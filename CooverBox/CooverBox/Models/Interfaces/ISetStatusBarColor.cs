﻿namespace CooverBox.Models.Interfaces
{
    public interface ISetStatusBarColor
    {
        void SetStatusBarColor();
    }
}
