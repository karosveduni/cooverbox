﻿using System;
using Xamarin.Forms;
using Xamarin.Essentials;

namespace CooverBox.Models
{
    internal class BaseCabinetViewModel : BaseViewModel
    {
        public BaseCabinetViewModel()
        {
            if (Start == null)
            {
                Preferences.Set("Exit", true);
                Start = () => Device.StartTimer(TimeSpan.FromSeconds(5), () =>
                {
                    Timer();
                    return Preferences.Get("Exit", false);
                });
                Start.Invoke();
            }
        }

        public event Action Timer = delegate { };
        public static Action Start = null;
    }
}
