﻿using System;
using Xamarin.Forms;

namespace CooverBox.Models
{
    internal class Order
    {
        public int ID { get; set; }
        public DateTime Create { get; set; }
        public DateTime Completed { get; set; }
        public string Name { get; set; }

        public Command DeleteItem => new Command(async () =>
        {
            Database.DeleteOrder(ID);
            await Application.Current.MainPage.DisplayAlert("Удаление", "Подождите...", "Закрыть окно");
        });

    }
}
