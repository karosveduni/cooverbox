﻿using CooverBox.ViewModels;
using Firebase.Auth;
using System.Linq;
using Xamarin.Forms;

namespace CooverBox.Models
{
    internal class RefreshUserDetail
    {
        public static async void SetDetail(string Parameter, string Value)
        {
            if (string.IsNullOrEmpty(Value) || string.IsNullOrWhiteSpace(Value))
            {
                await Shell.Current.DisplayAlert("Пустое поле", "Проверьте ввод данных.", "Закрыть окно");
                return;
            }
            FirebaseAuthProvider provider = new FirebaseAuthProvider(SignInViewModel.FirebaseConfig);
            Client client = null;
            switch (Parameter)
            {
                case "Email":
                    //_ = await provider.ChangeUserEmail( , Text);
                    break;
                case "Name":
                    _ = await provider.UpdateProfileAsync(SignInViewModel.FirebaseAuthLink.FirebaseToken, Value, SignInViewModel.FirebaseAuthLink.User.PhotoUrl);
                    break;
                case "AddOrder":
                    client = await Database.GetClientAsync();
                    int id = client.Orders?.Max(o => o.ID) + 1 ?? 0;
                    System.Collections.Generic.List<Order> order = client.Orders?.ToList() ?? new System.Collections.Generic.List<Order>();
                    order.Add(new Order { ID = id, Name = Value, Create = System.DateTime.Now });
                    client.Orders = order;
                    client.IsAddInformation = true;
                    Database.SetClient(client);
                    break;
                case "AddDesign":
                    client = await Database.GetClientAsync();
                    System.Collections.Generic.List<Design> designs = client.Designs?.ToList() ?? new System.Collections.Generic.List<Design>();
                    designs.Add(new Design { Image = null, URL = Value, DateCreate = System.DateTime.Now });
                    client.Designs = designs;
                    client.IsAddInformation = true;
                    Database.SetClient(client);
                    break;
                default:
                    break;
            }
        }
    }
}
