﻿using System;
using System.Collections;
using System.IO;
using System.Linq;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace CooverBox.Models
{
    public class Design
    {
        public Image Image { get; set; }
        public DateTime DateCreate { get; set; }
        public string URL { get; set; }
        public Command<IEnumerable> OpenViewer => new Command<IEnumerable>(async (ItemsSource) =>
            await Shell.Current.Navigation.PushAsync(new Views.Viewer(ItemsSource.Cast<Design>()), true));
        public Command Share => new Command(async () =>
        {
            StreamImageSource streamImage = (StreamImageSource)Image.Source;
            Stream stream = await streamImage.Stream(System.Threading.CancellationToken.None);

            string documentsPath = Path.GetTempPath();
            documentsPath = Path.Combine(documentsPath, "Share");
            _ = Directory.CreateDirectory(documentsPath);

            string filePath = Path.Combine(documentsPath, "share.png");

            byte[] bArray = new byte[stream.Length];
            using (FileStream fs = new FileStream(filePath, FileMode.OpenOrCreate))
            {
                using (stream)
                {
                    _ = stream.Read(bArray, 0, (int)stream.Length);
                }
                fs.Write(bArray, 0, bArray.Length);
            }

            await Xamarin.Essentials.Share.RequestAsync(new ShareFileRequest("Дизайн", new ShareFile(filePath)));
        });

        public Command DownloadCloud => new Command(async () =>
        {
            StreamImageSource streamImage = (StreamImageSource)Image.Source;
            Stream stream = await streamImage.Stream(System.Threading.CancellationToken.None);
            byte[] bArray = new byte[stream.Length];

            /*string documentsPath = Environment.GetFolderPath(Environment.SpecialFolder.MyPictures);
            documentsPath = Path.Combine(documentsPath, "CooverBox");
            _ = Directory.CreateDirectory(documentsPath);

            string filePath = Path.Combine(documentsPath, "tempFile.png");*/

            string filePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "notes.png");

            using (stream)
            {
                _ = stream.Read(bArray, 0, (int)stream.Length);
            }

            File.WriteAllBytes(filePath, bArray);
            System.Diagnostics.Debug.WriteLine(filePath);
            
        });

        public Command DeleteDesign => new Command(async () =>
        {
            await Database.DeleteDesign(URL);
        });
    }
}