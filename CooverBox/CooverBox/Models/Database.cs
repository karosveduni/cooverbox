﻿using CooverBox.ViewModels;
using Firebase.Database;
using Firebase.Database.Query;
using Firebase.Storage;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace CooverBox.Models
{
    internal class Database
    {
        private static readonly FirebaseClient firebase = new FirebaseClient("https://cooverbox-1e40f-default-rtdb.europe-west1.firebasedatabase.app/", new FirebaseOptions
        {
            AuthTokenAsyncFactory = () => Task.FromResult("HmaJtr9mH2PB6EZOvoVEmrGQvz5Ql2Ds6XKXKa9C"),
        });

        public const string Bucket = "cooverbox-1e40f.appspot.com";
        private static readonly FirebaseStorage storage = new FirebaseStorage(Bucket, new FirebaseStorageOptions
        {
            AuthTokenAsyncFactory = () => Task.FromResult(SignInViewModel.FirebaseAuthLink.FirebaseToken),
            ThrowOnCancel = false,
        });

        private static readonly string NameDataBase = "DataBase";

        public static async Task<Client> GetClientAsync()
        {
            return await firebase.Child($"{NameDataBase}/{SignInViewModel.FirebaseAuthLink?.User.LocalId}").OnceSingleAsync<Client>();
        }

        public static async void SetClient(Client client)
        {
            await firebase.Child(NameDataBase).Child(SignInViewModel.FirebaseAuthLink.User.LocalId).PutAsync(client);
        }
        public static async Task<bool> GetIsAddInformationAsync()
        {
            return await firebase.Child($"{NameDataBase}/{SignInViewModel.FirebaseAuthLink?.User.LocalId}").Child("IsAddInformation").OnceSingleAsync<bool>();
        }
        public static async void SetIsAddInformation(bool value = false)
        {
            await firebase.Child($"{NameDataBase}/{SignInViewModel.FirebaseAuthLink.User.LocalId}").Child("IsAddInformation").PutAsync(value);
        }
        public static async void DeleteOrder(int Id)
        {
            Client Client = await firebase.Child($"{NameDataBase}/{SignInViewModel.FirebaseAuthLink.User.LocalId}").OnceSingleAsync<Client>();
            List<Order> Orders = Client.Orders.ToList();
            _ = Orders.RemoveAll(or => or.ID == Id);
            Client.Orders = Orders;
            Client.IsAddInformation = true;
            await firebase.Child(NameDataBase).Child(SignInViewModel.FirebaseAuthLink.User.LocalId).PutAsync(Client);
        }
        public static async Task<string> UpdatePhotoAsync(Stream stream)
        {
            _ = await storage.Child(SignInViewModel.FirebaseAuthLink.User.LocalId).Child("Avatar/Photo.png").PutAsync(stream);
            return await storage.Child(SignInViewModel.FirebaseAuthLink.User.LocalId).Child("Avatar/Photo.png").GetDownloadUrlAsync();
        }

        public async Task<string> GetURLAvatarAsync()
        {
            return await storage.Child(SignInViewModel.FirebaseAuthLink.User.LocalId).Child("Avatar/Photo.png").GetDownloadUrlAsync();
        }
        public static async Task<IEnumerable<Design>> GetDesignsAsync()
        {
            Design[] result = await firebase.Child("DataBase").Child(SignInViewModel.FirebaseAuthLink.User.LocalId).Child("Designs").OnceSingleAsync<Design[]>();

            int index = 0;
            while (index < result.Length)
            {
                if (result[index].Image != null)
                    result[index].Image.Source = ImageSource.FromUri(new Uri(result[index].URL));
                else
                {
                    result[index].Image = new Image();
                    result[index].Image.Source = ImageSource.FromUri(new Uri(result[index].URL));
                }
                index++;
            }

            return result;
        }

        public static async Task<string> AddDesigenAsync(string url)
        {
            Stream stream = ViewModels.Cabinet.SettingViewModel.GetStreamFromUrl(url);
            string result = await storage.Child(SignInViewModel.FirebaseAuthLink.User.LocalId).Child($"Photo/{Path.GetRandomFileName()}.jpg").PutAsync(stream);
            return result;
        }

        public static async Task<string> AddDesigenAsync(Stream stream)
        {
            string result = await storage.Child(SignInViewModel.FirebaseAuthLink.User.LocalId).Child($"Photo/{Path.GetRandomFileName()}.jpg").PutAsync(stream);
            return result;
        }
        public static async Task DeleteDesign(string url)
        {
            string NameFile = url.Split(new string[] { "%2F", "?" }, StringSplitOptions.None)[2];
            await storage.Child(SignInViewModel.FirebaseAuthLink.User.LocalId).Child($"Photo/{NameFile}").DeleteAsync();
            IEnumerable<Design> designs = await firebase.Child("DataBase").Child(SignInViewModel.FirebaseAuthLink.User.LocalId).Child("Designs").OnceSingleAsync<IEnumerable<Design>>();
            List<Design> designsList = new List<Design>(designs);
            _ = designsList.RemoveAll(d => d.URL == url);
            await firebase.Child("DataBase").Child(SignInViewModel.FirebaseAuthLink.User.LocalId).Child("Designs").PutAsync<IEnumerable<Design>>(designsList);
            SetIsAddInformation(true);
        }
    }
}