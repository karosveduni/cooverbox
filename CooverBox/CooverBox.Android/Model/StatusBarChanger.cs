﻿using Android.OS;
using Android.Views;
using CooverBox.Droid.Model;
using Xamarin.Essentials;
using Xamarin.Forms;

[assembly: Dependency(typeof(StatusBarChanger))]
namespace CooverBox.Droid.Model
{
    public class StatusBarChanger : Models.Interfaces.ISetStatusBarColor
    {
        [System.Obsolete]
        public void SetStatusBarColor()
        {
            if (Build.VERSION.SdkInt < BuildVersionCodes.Lollipop) return;

            var windows = ((MainActivity)Forms.Context).Window;
            windows.AddFlags(WindowManagerFlags.DrawsSystemBarBackgrounds);
            windows.ClearFlags(WindowManagerFlags.TranslucentStatus);
            windows.SetStatusBarColor(System.Drawing.Color.FromArgb(213, 189, 153).ToPlatformColor());
        }
    }
}