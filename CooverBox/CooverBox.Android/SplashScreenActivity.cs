﻿using Android.App;
using Android.Content;
using Android.OS;

namespace CooverBox.Droid
{
    [Activity(Label = "Coover Box", MainLauncher = true, Theme = "@style/MyTheme.Splash", NoHistory = true)]
    public class SplashScreenActivity : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        protected override void OnResume()
        {
            base.OnResume();
            StartActivity(new Intent(Application.Context, typeof(MainActivity)));
        }
    }
}