﻿namespace CooverBox.UWP
{
    public sealed partial class MainPage
    {
        public MainPage()
        {
            InitializeComponent();

            LoadApplication(new CooverBox.App());
        }
    }
}
